# frozen_string_literal: true

require 'discordrb'
require 'json'
require 'tempfile'

class RoomService
  BotId = 721035168009420840
  Mention = "<@#{BotId}>"
  Text = 0
  Category = 4
  Count = 10

  class Guild < Struct.new(
    :name, :place, :members_count, :average_power,
    :guild_name, :position,
    :title,
    keyword_init: true)
    def total_troops
      if members_count && average_power
        total = members_count * average_power
        if total > 1000_000
          sprintf('%.2fM', total / 1000_000.0)
        elsif total > 100_000
          sprintf('%dk', total / 1000)
        elsif total > 1000
          sprintf('%.2fk', total / 1000.0)
        else
          total
        end
      else
        '?'
      end
    end

    def name
      super || guild_name
    end

    def place
      super || position
    end
  end

  class Session < Struct.new(:bot, :event, :prefix)
    def initialize *args
      super
      self.server = event.server
    end

    def rebuild_everything
      [
        delete_roles,
        delete_channels,
        assign_guilds
      ].inject(&:&)
    end

    def attach role_name
      if role = roles.find{ |r| r.name == role_name }
        RoomService.send_message(event,
          "Attaching `#{role_name}` to all `#{category_name}` channels")

        each_prefix_channel do |chan|
          chan.define_overwrite(
            Discordrb::Overwrite.new(role, allow: permissions))
        end

        true
      else
        RoomService.send_message(event, "! Cannot find role `#{role_name}`")
        false
      end
    end

    def attach_observer role_name
      if role = roles.find{ |r| r.name == role_name }
        RoomService.send_message(event,
          "Attaching `#{role_name}` as observers to all `#{category_name}` channels")

        each_prefix_channel do |chan|
          chan.define_overwrite(
            Discordrb::Overwrite.new(role, allow: observer_permissions))
        end

        true
      else
        RoomService.send_message(event, "! Cannot find role `#{role_name}`")
        false
      end
    end

    def detach role_name
      if role = roles.find{ |r| r.name == role_name }
        RoomService.send_message(event,
          "Detaching `#{role_name}` from all `#{category_name}` channels")

        each_prefix_channel do |chan|
          chan.delete_overwrite(role)
        end

        true
      else
        RoomService.send_message(event, "! Cannot find role `#{role_name}`")
        false
      end
    end

    def assign_guilds
      RoomService.send_message(event,
        "Assigning guilds to their corresponding `#{category_name}` channel")

      if url = event.message.attachments.first&.url
        if guilds = fetch_guilds(url)
          process_guilds(guilds)
          @all_channels = nil # Refresh because channel overwrite changes
          hint_missing_guilds
          true
        else
          RoomService.send_message(event,
            "! The attached file doesn't seem right")
          false
        end
      else
        RoomService.send_message(event,
          "! You have to attach the guilds JSON file")
        false
      end
    end

    def assign_roles
      RoomService.send_message(event,
        "Assigning roles to their corresponding `#{category_name}` channel" \
        " and guild members")

      role_by_name = roles.group_by(&:name)
      guild_by_role_id = role_id_to_guild_roles

      each_prefix_channel do |chan|
        role = ensure_role(role_by_name, chan.name)

        chan.define_overwrite(
          Discordrb::Overwrite.new(role, allow: permissions))

        chan.role_overwrites.each do |overwrite|
          if guild_role = guild_by_role_id[overwrite.id]
            assign_members_with(guild_role.members, role)
          end
        end
      end

      true
    end

    def delete_roles
      RoomService.send_message(event,
        "Deleting roles starting with `#{category_name}`")

      roles.delete_if do |r|
        r.name.start_with?(category_name) && r.delete
      end

      true
    end

    def delete_channels
      RoomService.send_message(event,
        "Deleting channels starting with `#{category_name}`")

      channels.delete_if do |chan|
        chan.name.start_with?(category_name) && chan.delete
      end

      categories.delete_if{ |cat| cat.name == category_name && cat.delete }

      true
    end

    def broadcast
      RoomService.send_message(event,
        "Broadcasting to corresponding `#{category_name}` channel")

      if event.message.attachments.any?
        event.message.attachments.map(&method(:broadcast_attachment)).all?
      else
        RoomService.send_message(event,
          "! You have to attach the broadcasting messages")
        false
      end
    end

    def pin message_id
      Discordrb::API::Channel.
        pin_message(bot.token, event.channel.id, message_id)
    rescue Discordrb::Errors::NoPermission
      RoomService.send_message(event,
        "! I do not have permission for managing messages")
    end

    def attach_overlay overlay_url, screenshot_url
      if screenshot_url ||= event.message.attachments.first&.url
        download_image('screenshot', screenshot_url) do |screenshot|
          download_image('overlay', overlay_url) do |overlay|
            composite_images(screenshot, overlay) do |result_map|
              RoomService.send_file(event, result_map)
              true
            end
          end
        end
      else
        RoomService.send_message(event,
          "! You have to attach the screenshot to the map")
        false
      end
    end

    private

    attr_accessor :server

    def fetch_guilds url
      ratings = JSON.parse(Discordrb::API.raw_request(:get, url))
      guilds = ratings.dig('result', 'guilds')
      guild_fields = Guild.members.map(&:to_s)

      case guilds
      when Array
        return unless guilds.all?{ |g| g.kind_of?(Hash) }

        guilds.inject({}) do |result, guild_hash|
          guild = Guild.new(guild_hash.slice(*guild_fields))
          result[guild.place.to_i] = guild
          result
        end
      end
    rescue JSON::ParserError
    end

    def process_guilds guilds
      each10_guilds(guilds) do |each10, index|
        channel_name = generate_name(index)

        if channel = channels.find{ |c| c.name == channel_name }
          process_guilds_for(each10, channel)
        else
          RoomService.send_message(event,
            "Cannot find channel for `#{channel_name}`. Creating on demand")
          channel = create_single_channel(index)
          process_guilds_for(each10, channel)
        end
      end
    end

    def each10_guilds guilds # {1 => Guild}
      1.upto(guilds.max.first).each_slice(10).with_index do |ranks, index|
        each10 = ranks.map(&guilds.method(:[])) # may have empty spots

        yield(each10, index) if each10.any?(&:itself)
      end
    end

    def create_single_channel index
      category = categories.find do |cat|
        cat.name == category_name
      end

      category ||= begin
        RoomService.send_message(event,
          "Cannot find category #{category_name}. Creating on demand")
        server_create_category(category_name)
      end

      name = generate_name(index)
      channel_role = server_create_role(name: name, permissions: 0)

      perm = permissions
      channel_overwrite = Discordrb::Overwrite.new(channel_role, allow: perm)
      everyone = Discordrb::Overwrite.new(server.everyone_role, deny: perm)
      room_service = Discordrb::Overwrite.new(bot.profile, allow: perm)

      server_create_channel(name, parent: category,
        permission_overwrites: [channel_overwrite, everyone, room_service])
    end

    def process_guilds_for each10, channel
      process_guilds_for_roles(each10, channel)
      process_guilds_for_map(each10, channel)
    end

    def process_guilds_for_roles each10, channel
      role_by_name = roles.group_by(&:name)
      announcement = each10.compact.map do |guild| # ignore empty spots
        guild_name_stripped = guild.name.strip

        guild_title =
          if guild_role = roles.find{ |r| r.name == guild_name_stripped }
            RoomService.send_message(event,
              "Assigning `#{guild.name}` to #{channel.mention}")

            channel.define_overwrite(
              Discordrb::Overwrite.new(guild_role, allow: permissions))

            role = ensure_role(role_by_name, channel.name)
            assign_members_with(guild_role.members, role)

            guild_role.mention
          else
            "`#{guild.name}`"
          end

        # So we can use this in process_guilds_for_map
        guild.title = guild_title

        "#{guild.place}. #{guild_title} - `#{guild.total_troops}`"
      end.join("\n")

      RoomService.send_message(channel,
        "This channel is accessible for the following guilds:" \
        "\n#{announcement}")
    end

    def process_guilds_for_map each10, channel
      entries = {
        'A1' => each10.values_at(5, 9),
        'A2' => each10.values_at(6, 7, 8),
        'A3' => each10.values_at(3, 4, 9),
        'A4' => each10.values_at(0, 1, 5),
        'A5' => each10.values_at(2, 7, 8),
        'A6' => each10.values_at(3, 6),
        'B1' => each10.values_at(0, 2, 4),
        'B6' => each10.values_at(0, 1, 7),
        'C1' => each10.values_at(3, 6, 8),
        'C6' => each10.values_at(2, 5, 8),
        'D1' => each10.values_at(1, 5, 9),
        'D6' => each10.values_at(4, 6, 9),
        'E1' => each10.values_at(2, 7),
        'E2' => each10.values_at(0, 3, 4),
        'E3' => each10.values_at(1, 5, 8),
        'E4' => each10.values_at(2, 6, 9),
        'E5' => each10.values_at(3, 4, 7),
        'E6' => each10.values_at(0, 1)
      }

      entry_points = entries.map do |position, guilds|
        guild_list = guilds.compact.map(&:title).join(', ')

        "* #{position}: #{guild_list}"
      end.join("\n")

      RoomService.send_message(channel, "Entry points:\n#{entry_points}")

      generate_overlay(entries) do |overlay|
        RoomService.send_file(channel, overlay)
      end
    end

    def hint_missing_guilds
      guild_by_role_id = role_id_to_guild_roles

      each_prefix_channel do |chan|
        chan.role_overwrites.each do |overwrite|
          guild_by_role_id.delete(overwrite.id)
        end
      end

      if guild_by_role_id.any?
        missing_guilds = guild_by_role_id.each_value.map do |guild_role|
          "`#{guild_role.name}`"
        end.sort.join(', ')

        RoomService.send_message(event,
          "Missing `#{category_name}` for #{missing_guilds}")
      end
    rescue JSON::ParserError
      RoomService.send_message(event, "!!! BUG. Check `guilds.json`")
    end

    def ensure_role role_by_name, name
      role_by_name[name]&.first || begin
        RoomService.send_message(event,
          "Cannot find role `#{name}`. Creating on demand")
        server_create_role(name: name, permissions: 0)
      end
    end

    def assign_members_with members, role
      member_list = members.map do |mem|
        "`#{mem.display_name}`"
      end.join(', ')

      RoomService.send_message(event,
        "Assigning `#{role.name}` to #{member_list}")

      members.each do |mem|
        mem.add_role(role)
      end
    end

    def role_id_to_guild_roles
      all_guilds =
        JSON.parse(File.read(File.expand_path('guilds.json', __dir__))).
          transform_keys(&:strip)
          # So we can compare with guild_name_stripped

      roles.inject({}) do |result, r|
        result[r.id] = r if all_guilds.key?(r.name)
        result
      end
    end

    def server_create_role **args
      server.create_role(**args).tap(&roles.method(:<<))
    end

    def server_create_channel name, **args
      server.create_channel(name, Text, **args).tap(&channels.method(:<<))
    end

    def server_create_category name
      server.create_channel(name, Category).tap(&categories.method(:<<))
    end

    def each_prefix_channel
      channels.each do |chan|
        yield(chan) if chan.name.start_with?(category_name)
      end
    end

    def roles
      @roles ||= request_map(:roles, Discordrb::Role)
    end

    def channels
      all_channels[:channels]
    end

    def categories
      all_channels[:categories]
    end

    def all_channels
      @all_channels ||= begin
        chans = request_map(:channels, Discordrb::Channel).group_by(&:type)

        {
          categories: chans[Category] || [],
          channels: chans[Text] || []
        }
      end
    end

    # We have to do this to get the fresh list, and `server.roles` simply
    # cannot return all the roles.
    def request_map meth, klass
      json = Discordrb::API::Server.public_send(meth, bot.token, server.id)

      JSON.parse(json).map{ |response| klass.new(response, bot, server) }
    end

    def permissions
      @permissions ||= Discordrb::Permissions.new(%i[
        add_reactions
        read_messages
        send_messages
        embed_links
        attach_files
        read_message_history
        use_external_emoji
      ]).freeze
    end

    def observer_permissions
      @observer_permissions ||= Discordrb::Permissions.new(%i[
        read_messages
        read_message_history
      ]).freeze
    end

    def category_name
      @category_name ||= generate_name
    end

    def generate_name index=nil
      if index
        "#{prefix} #{index * Count + 1}"
      else
        prefix
      end.downcase.tr(' ', '-')
    end

    def broadcast_attachment attachment
      download_file('broadcast', attachment.url) do |file_path|
        each_prefix_channel do |chan|
          if `file #{file_path}`.match?(/\btext\b/i)
            RoomService.send_message(chan, File.read(file_path))
          else
            RoomService.send_file(chan, File.open(file_path))
          end
        end
      end
    end

    def generate_overlay entries
      magick_args = entries.flat_map.with_index do |(position, guilds), rank|
        y = position[0].ord - 'A'.ord
        x = position[1].ord - '1'.ord

        guilds.flat_map.with_index do |g, i|
          ['-annotate',
           "+#{x * 215 + 5}+#{y * 160 + i.succ * 28}",
           g&.name || "?#{rank}"]
        end
      end

      with_tempfile('overlay', '.png') do |overlay|
        system('magick',
          '-size', '1280x800',
          'canvas:transparent',
          '-pointsize', '25',
          '-undercolor', 'white',
          *magick_args,
          overlay.path)

        yield(File.open(overlay.path))
      end
    end

    def download_image name, url
      download_file(name, url) do |image_path|
        if system('identify', image_path, [:out, :err] => File::NULL)
          yield(File.open(image_path))
        else
          RoomService.send_message(event, "! #{name} is not an image")
          false
        end
      end
    end

    def composite_images screenshot, overlay
      screenshot_dimension = `identify -format '%wx%h' #{screenshot.path}`

      with_tempfile('map', '.jpg') do |map|
        system('magick',
          screenshot.path, '(',
          overlay.path, '-scale', "#{screenshot_dimension}!",
          ')', '-composite', map.path)

        yield(File.open(map.path))
      end
    end

    def download_file name, url
      with_tempfile(name, File.extname(url)) do |file|
        system('wget', '-O', file.path, url)

        yield(file.path)
      end
    end

    def with_tempfile *filename
      tempfile = Tempfile.new(filename)

      yield(tempfile)
    ensure
      tempfile.close
      tempfile.unlink
    end
  end

  attr_reader :bot, :mutex

  def initialize
    @bot = Discordrb::Bot.new(token: ENV['DISCORD_TOKEN'])
    @mutex = Mutex.new
  end

  def hook
    syntax = /<@!?#{BotId}> *(.*)/i

    bot.message(containing: syntax) do |event|
      admin = admin?(event.user.on(event.server))

      success =
        event.message.content.each_line.inject(true) do |result, line|
          if command = line[syntax, 1]
            dispatch(command, event, admin) & result
          else
            result
          end
        end

      if success
        RoomService.send_message(event,
          "#{event.user.mention} Requests have been done. " \
          "Please rate me :star: :star: :star: :star: :star:")
      end
    end
  end

  def run
    bot.run
  end

  private

  def dispatch message, event, admin
    session = Session.new(bot, event)

    dispatch_for_guests(session, message, admin) do
      if admin
        dispatch_for_admins(session, message)
      else
        RoomService.send_message(event,
          "#{event.user.mention} I respond to admin only!")
        false
      end
    end
  end

  def dispatch_for_guests session, message, admin
    case message
    when /\Ahelp\b/, ''
      if admin
        RoomService.send_message(session.event, <<~MARKDOWN)
          Usage #{Mention}:
          #{admin_commands.chomp}
          #{guest_commands}
        MARKDOWN
      else
        RoomService.send_message(session.event, <<~MARKDOWN)
          Usage #{Mention}:
          #{guest_commands}
        MARKDOWN
      end
      false
    when /\Apin +(\d+)/
      session.pin($1)
      false
    when /\A(overlay +)?(https?:\S+) *(https?:\S+)?/
      session.attach_overlay($2, $3)
      false # Be quiet for this
    when /\Ainvite\b/
      RoomService.send_message(session.event,
        'https://discord.com/oauth2/authorize?client_id=721035168009420840&scope=bot&permissions=268823632'
      )
      false
    else
      yield if block_given?
    end
  end

  def dispatch_for_admins session, message
    case message
    when /\Arebuild +everything +(.+)/
      session.prefix = $1
      session.rebuild_everything
    when /\Aattach +(".+")+ +(.+)/
      session.prefix = $2
      $1.scan(/"(.+?)"/).flatten.map(&session.method(:attach)).all?
    when /\Aattach observer +(".+")+ +(.+)/
      session.prefix = $2
      $1.scan(/"(.+?)"/).flatten.map(&session.method(:attach_observer)).all?
    when /\Adetach +(".+")+ +(.+)/
      session.prefix = $2
      $1.scan(/"(.+?)"/).flatten.map(&session.method(:detach)).all?
    when /\Aassign +guilds +(.+)/
      session.prefix = $1
      session.assign_guilds
    when /\Aassign +roles +(.+)/
      session.prefix = $1
      session.assign_roles
    when /\Adelete +roles +(.+)/
      session.prefix = $1
      session.delete_roles
    when /\Adelete +channels +(.+)/
      session.prefix = $1
      session.delete_channels
    when /\Abroadcast +(.+)/
      session.prefix = $1
      session.broadcast
    else
      RoomService.send_message(session.event,
        "#{session.event.user.mention} Pardon? Did you miss [PREFIX]?" \
        "\nPing me without anything else to get help")
      false
    end
  end

  def admin_commands
    <<~MARKDOWN
      **rebuild everything [PREFIX]**
        - This will delete all corresponding roles and channels and start over. *It runs `delete roles`, `delete channels`, and then `assign guilds`*
      **attach "[ROLE NAME]"+ [PREFIX]**
        - This will attach the specified role(s) to all the corresponding channels
      **attach observer "[ROLE NAME]"+ [PREFIX]**
        - This will attach the specified role(s) in a read-only way to all the corresponding channels
      **detach "[ROLE NAME]"+ [PREFIX]**
        - This will detach the specified role(s) to all the corresponding channels
      **assign guilds [PREFIX]**
        - This will require the JSON file as the attachment file and will assign the guilds to the corresponding channels, and assign roles to the corresponding guild members. *This will be useful when you want to assign additional guilds*
      **assign roles [PREFIX]**
        - This will assign the corresponding roles to the corresponding channels and corresponding guild members. *This will be useful when you want to fix the roles without touching anything else*
      **delete roles [PREFIX]**
        - This will delete all the corresponding roles. *This will be useful when you want to fix the roles by deleting them first and then assign back with `assign roles`*
      **delete channels [PREFIX]**
        - This will delete all the corresponding channels
      **broadcast [PREFIX]**
        - This will broadcast the attached messages or files to each channels
    MARKDOWN
  end

  def guest_commands
    <<~MARKDOWN
      **pin [MESSAGE ID]**
        - This will pin the message
      **(overlay) [URL] [URL]**
        - This will composite the overlay over the map
      **invite**
        - This shows the invitation URL for the bot
    MARKDOWN
  end

  def admin? member
    member.owner? || member.roles.any? do |role|
      role.permissions.administrator
    end
  end

  def self.send_message object, message
    retry_request do
      object.send_message(message[0, 2000])
    end
  end

  def self.send_file object, file
    retry_request do
      object.send_file(file)
    end
  end

  def self.retry_request retries=3
    yield
  rescue RestClient::Exception => e
    puts "Error: #{e.class}:#{e.message}, retries: #{retries}"

    if retries > 0
      sleep(0.1)
      retry_request(retries - 1)
    end
  end
end

bot = RoomService.new
bot.hook
bot.run
